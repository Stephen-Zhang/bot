from .soldier import Soldier
from ._main import DIRS, DIRS2
from .arms import Arms, Arms2
from .hero import Hero, Hero2
from .settings import PAGES
from .faction import Faction
from .MapImage import MapImage