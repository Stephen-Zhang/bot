from ._main import DIRS,Embed, DIRS2, SkillDescriptionAIO, convert_HTML

def Soldier(iname):
	#get soldier dir
	soldier=DIRS['soldier'][iname]
	#create basic embed
	embed= Embed(
		title='%s (%s)'%(soldier['name'],soldier['type']), #page name
		url=soldier['url']  #page link
		)
	embed.set_thumbnail(url=soldier['img'])

	SingleStats=['Span','Move','S','W',]

	fields=([
		{'name':'Move'   ,'value': soldier['stats']['Move'] ,'inline':True},
		{'name':'Range'   ,'value':soldier['stats']['Span'] ,'inline':True},
	])
	if 'S' in soldier['stats']:
		fields.append({'name':'Strong Against'   ,'value':soldier['stats']['S'] ,'inline':True})
	if 'W' in soldier['stats']:  
		fields.append({'name':'Weak Against'   ,'value':soldier['stats']['W'] ,'inline':True})
	if 'expr' in soldier and len(soldier['expr'])>2:
		fields.append({'name':'Skill'   ,'value':soldier['expr'] ,'inline':True})
	fields+=[
		{'name':'Stats'   ,'value':', '.join(['%s %s'%(val,key) for key,val in soldier['stats'].items() if key not in SingleStats]) ,'inline':False},
		#{'name':''   ,'value':'' ,'inline':True}
	]
	embed.ConvertFields(fields)
	return embed