from datetime import datetime,timedelta
from discord.message import Embed
from tabulate import tabulate
import os, json, re
#variables
if os.name == 'nt':
	resPath=  os.path.join(os.path.realpath(__file__),*[os.pardir,os.pardir,os.pardir,'res'])
else:
	resPath=  os.path.join('res')
dbpath= os.path.join(resPath,'Database.json')	#os.path.join(os.path.realpath(__file__),*[os.pardir,os.pardir,os.pardir,'res','Database.json'])

with open(dbpath,'rb') as f:
	DIRS=json.loads(f.read())

DIRS2={}
for fp in os.listdir(os.path.join(resPath,'ConfigData2')):
	try:
		name=os.path.basename(fp).split('.')[0]
		DIRS2[name]={
			item['ID']:item
			for num,item in json.loads(open(os.path.join(resPath,*['ConfigData2',fp]),'rb').read()).items()
		}
	except Exception as e:
		print(e)
# for fp in os.listdir(os.path.join(resPath,'ConfigData')):
# 	try:
# 		name=os.path.basename(fp).split('.')[0]
# 		DIRS2[name[10:-4]]={
# 			item['ID']:item
# 			for item in json.loads(open(os.path.join(resPath,*['ConfigData',fp]),'rb').read())
			
# 		}
# 	except Exception as e:
# 		print(e)
#fix enum names
for key,items in DIRS2.items():
	for skey,var in items.items():
		if var:
			if type(var)==list and type(var[0])==str and '_' in var[0]:
				DIRS2[key][skey]=[item.split('_',1)[1] for item in var]
			elif type(var)==str and '_' in var: 
				DIRS2[key][skey]=var.split('_',1)[1]

def convert_HTML(text):
	#tags to markdown

	#remove other tags
    return re.sub(r'<.+?>','',text)


def gitlink(ipath,sub='ExportAssetBundle'):
	url='https://gitlab.com/langrisser/assets/raw/master/%s/%s'
	ipath=ipath.replace('_ABS','').split('/')
	ipath[:-1]=[item.lower() for item in ipath[:-1] if item]
	return url%(sub,'/'.join(ipath))

def Rarity(base, max):
	base+=1
	max+=1
	return "★"*base + "☆"*(max-base)

def SkillDescriptionAIO(ids):
	#filter the skill values
	r = re.compile('>(.+?)<')
	vals=[
		[val for i,val in enumerate(r.findall(DIRS2['Skill'][_id]['Desc'])) if not i%2]
		for _id in ids
	]
	#create aio string
	desc=re.split(r'<.+?>',DIRS2['Skill'][ids[-1]]['Desc'])
	for i in range(len(vals[0])):
		cvals=[val[i] for val in vals]
		desc[i*2+1]='/'.join(cvals) if len(set(cvals)) > 1 else cvals[0]
	return '**'.join(desc)
		

#Embed patch
def ConvertFields(self,fields):
	if fields:
		print(fields)
		for field in fields:
			if 'inline' not in field:
				field['inline']=True
			if type(field['value'])!=str:
				field['value']=str(field['value'])
			for string in ['name','value']:
				if len(field[string])>255:
					field[string]=field[string][:256]

		if not getattr(self,'_fields',False):
			self._fields=[]
		self._fields += [
				field
				for field in fields
				if len(str(field['name']).rstrip(' '))>0 and len(str(field['value']).rstrip(' '))>0
			]
Embed.ConvertFields=ConvertFields

#functions
def createTable(data):
	# print tabulate([["value1", "value2"], ["value3", "value4"]], ["column 1", "column 2"], tablefmt="grid")
	# +------------+------------+
	# | column 1   | column 2   |
	# +============+============+
	# | value1     | value2	    |
	# +------------+------------+
	# | value3     | value4	    |
	# +------------+------------+
	if type(data)==dict:
		headers=[]
		values=[[]]
		for key,val in data.items():
			headers.append(key)
			values[0].append(val)
	elif type(data)==list and type(data[0])==dict:
		headers=[]
		for obj in data:
			for key in obj:
				if key not in headers:
					headers.append(key)
		values=[
			[
				obj[key] if key in obj else None
				for key in headers
			]
			for obj in data
		]
	else:	#list
		headers=[i+1 for i in range(len(data))]
		values=[data]

	return tabulate(values,headers,tablefmt='grid')