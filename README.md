# Liara ~ a discord bot for Langrisser

The bot shows information about diverse parts of the game Langrisser in discord.
It runs via discord.py

## Requirements
Python 3.7
* discord.py
* jellyfish
* pillow
* tabulate

note:
    discord.py has to be installed via
    pip install git+https://github.com/Rapptz/discord.py.git@rewrite#egg=discord.py

## Structure
Liana.py is the main script which starts the bot.
It loads modules from /cogs.
Those modules react to discord events.
Some of their either shared or too big functions are located in /utils.

### cogs
* Langrisser.py     ~ module for the Langrisser content
* Spy.py            ~ diverse commands, some for general informations about the server or bot
* TAC-Discord.py    ~ moderation modul from the TAC bot, needs some reworks for overall use

### res
* Database.json     ~ data of the official Langrisser wiki
* /ConfigData       ~ datamined game data

Images and other files are stored in [assets](https://gitlab.com/langrisser/assets).
Those files are generated via [database](https://gitlab.com/langrisser/database).

### utils
* FindBest.py       ~ function which finds the best match for an input
* /Langrisser       ~ functions which generate the reply to Langrisser commands

### New Feature Requests

* Adding items required for classes level ups to the classes
* Adding class master bonuses to the classes
* Max stats for the hero and classes
* Fixing skill descriptions
* Command for where items drop in game from quests
